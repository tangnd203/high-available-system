# High Available System

## A. High Available ( HA )
### 1. What is HA ?
- High Available - tính có sẵn cao : là khả năng hoạt động một cách liên tục và không bị gián đoạn trong mọi tình huống của hệ thống
- Mục điêu của HA : đảm bảo dịch vụ hoặc hệ thống luôn luôn hoạt động và có khả năng chịu tải cao ngay cả khi xảy ra sự cố hoặc lỗi về phần cứng, phần mềm hoặc mạng

### 2. Why is HA important ?
- Đảm bảo tính liên tục của ứng dụng và dịch vụ : đảm bảo các hệ thống, tính năng, chức năng quan trọng luôn hoạt động liên tục và không gây ra sự gián đoạn khi gặp sự cố hoặc lỗi
- Tăng hiệu suất và khả năng chịu tải : cho phép phân phối tải các request và người dùng trên nhiều server để tăng hiệu suất và khả năng chịu tải => tận dụng tối đa tài nguyên có sẵn , tránh quá tải và sự chậm trễ trong việc xử lý request từ người dùng
- Bảo mật dữ liệu : khi một server gặp sự cố , hệ thống HA sẽ chuyển tiếp yêu cầu và dữ liệu sang server hoạt động bình thường khác mà không gây mất mát dữ liệu hoặc rò rỉ thông tin quan trọng
### 3. Types of HA
- Active - Passive : hệ thống backup được giữ ở chế độ passive => chỉ hệ thống primary fials

- Active - Active : nhiều hệ thống active thực thi và chia sẻ workload cho nhau => nếu một hệ thống fails thì hệ thống khác tự động nhận workload đó => cấu hình phức tập nhưng hiệu suất được cải thiện và khả năng mở rộng cao
### 4. How is HA implemented ?
- redundant component : triển khai nhiều server, kết nối mạng, hệ thống lưu trữ và nguồn điện trong hệ thống
- monitoring and alerts : thường xuyên giám sát hiệu suất và tính khả dụng => khi gặp sự cố thì cảnh báo (alerts) được tạo => sysadmin nhanh chóng nhận dạng và giải quết vấn đề => giảm nguy cơ downtime
- load balancing : một hoạc nhiều server chuyên dụng chặn các request đến một nhóm các hệ thống (backend) => phân phối traffic giữa các hệ thống backend để có hiệu suất tối ưu => nếu 1 hệ thống fails thì bộ load balancer sẽ tự động chuyển hướng các request qua các hệ thống khác đang hoạt động bình thường
- failover mechanism : triển khai cấu hình hệ thống active - passive hoặc active - active hoặc phân cụm chuyển đổi dự phòng để đảm bảo rằng khi một hệ thống fails thì hệ thống khác có thể takes over với sự gián đoạn tối thiểu
- backup and recovery system : đam bảo dữ liệu và ứng dụng cso thể nhanh chóng được phục hồi khi có lỗi xảy ra
## B. Load Balancer
### 1. What is Load Balancer ?
- Load balancer là một thành phần trong kiến trúc hệ thống mạng ⇒ phân phối request đến các máy chủ có khả năng đáp ứng các request đó với tối đa hóa tốc độ và khả năng tận dụng công suất ⇒ đảm bảo ko có  server nào bị quá tải
### 2. How does Load Balancer work ?
- Nhận yêu cầu: Load balancer nhận yêu cầu từ người dùng thông qua tên miền hoặc địa chỉ IP
- Phân phối tải: Load balancer sử dụng một thuật toán cân bằng tải để quyết định máy chủ nào trong nhóm máy chủ sẽ nhận yêu cầu
- Định tuyến yêu cầu: Load balancer chuyển tiếp yêu cầu từ người dùng đến máy chủ được chọn dựa trên thuật toán cân bằng tải. Yêu cầu được gửi đến máy chủ thông qua giao thức mạng (ví dụ: HTTP)
- Nhận và xử lý yêu cầu: Máy chủ nhận yêu cầu từ load balancer, xử lý nó và trả về kết quả cho người dùng qua load balancer
- Load balancer nhận kết quả và phản hồi cho client: Load balancer nhận kết quả từ máy chủ và chuyển lại cho người dùng. Kết quả có thể là dữ liệu hoặc mã trạng thái để người dùng có thể tiếp tục tương tác với ứng dụng
### 3. Load Balancing Algorithms 
- static 
    + phân phối workload mà không quan tâm đến trạng thái hiện tại của hệ thống
    + phân phối workload dựa trên kế hoạch định trước
    + setup nhanh nhưng thiếu hiệu quả
- dynamic
  + phân phối workload quan tâm đến tính có sẵn, tình trạng hiện tại của từng server
  + tự động chuyển traffic từ server quá tải , hoạt động kém sang server hoạt động tốt hơn
  + cấu hình phức tạp hơn nhưng sẽ đem lại hiệu quả cao hơn 

- common algorithms
    + least connection : xác định server hiện tại nào có số xử lý yêu cầu hiện tại ít nhất thì sẽ điều hướng traffic đến server đó
    + round-robin : cân bằng tải mạng luân chuyển luân phiên các request của người dùng trên các server theo chu kỳ
    + ip hashing : kết hợp địa chỉ IP nguồn và đích của incoming traffic và sử dụng hàm toán học để chuyển đổi chúng thành giá trị băm ⇒ hữu ích khi kết nối bị mất và cần được trả về máy chủ đã xử lý nó ban đầu
## C. Keepalived
### 1. What is Keepalived
- keepalived cung cấp frameworks cho cả load balancer và high availability của hệ thống
### 2. Main functions 
- kiểm tra tình trạng hệ thống LVS (Linux Virtual Server)
- triển khai ngăn xếp VRRPv2 (Virtual Redundancy Routing Protocol version 2) để xử lý chuyển đổi dự phòng load balancer
### 3. Failover (VRRP) framework
- là một giao thức bầu cử tự động giao trách nhiệm cho một trong các router VRRP có trong mạng LAN, router VRRP nào giữ các địa chỉ IP được kết nối với một virtual router được gọi là master và chuyển các packets đến các địa chỉ IP này
- quá trình bầu cử cung cấp tính năng chuyển đổi động khi master ko còn khả dụng ⇒ cho phép bất kỳ địa chỉ IP nào của virtual router trong mạng LAN cũng có thể được sử dụng như router mặc định đầu tiên bởi các máy tính kết nối
- VRRP tạo ra một đường mạng mặc định có tính sẵn sàng cao mà không cần phải cấu hình giao thức định tuyến động hoặc giao thức discovery router trên mọi máy tính kết nối
## D. Setup
### 1 - Mô hình
![alt text](/images/image.png)
- webserver 1 : 
- webserver 2 : 
- load balancer 1 : 
- load balancer 2 : 
- floating IP (keepalived) : 
### 2 - Cài đặt Nginx trên 4 máy
```
  $ sudo apt update
  $ sudo apt install nginx -y
```
### 3 - Cấu hình file /etc/nginx/sites-available/default trên 2 máy load balancer
```
  $ sudo vim /etc/nginx/sites-available/default
```
```bash
  server {
    listen 80;
    server_name 192.168.188.100;
    location / {
        proxy_pass http://backend;
    }
  }
```
### 4 - Cấu hình file /etc/nginx/nginx.conf của 2 máy load balancer
```
  $ sudo vim /etc/nginx/nginx.conf
```
```bash
  http {
    …
    upstream backend {
        server 192.168.188.128;
        server 192.168.188.129;
    }
  }
```
### 5 - Cài đặt keepalived trên 2 máy load balancer
```
  $ sudo apt update
  $ sudo apt install keepalived -y 
```
### 6 - Tạo và cấu hình file /etc/keepalived/keepalived.conf trên 2 máy load balancer
```
  $ sudo vim /etc/keepalived/keepalived.conf
```
- cấu hình máy Load balancer 1
```bash
  global_defs {
      router_id LVS_DEVEL
  }
  vrrp_script check_nginx {
      script "killall -0 nginx"
      interval 2
      weight 50
  }
  vrrp_instance VI_1 {
      state MASTER
      interface ens33
      virtual_router_id 51
      priority 110
      advert_int 1
      virtual_ipaddress {
          192.168.188.100
      }
      track_script {
          check_nginx
      }
  }
```
- cấu hình máy Load balancer 2
```bash
  global_defs {
      router_id LVS_DEVEL
  }
  vrrp_script check_nginx {
      script "killall -0 nginx"
      interval 2
      weight 50
  }
  vrrp_instance VI_1 {
      state BACKUP
      interface ens33
      virtual_router_id 51
      priority 100
      advert_int 1
      virtual_ipaddress {
          192.168.188.100
      }
      track_script {
          check_nginx
      }
  }
```
- Giải thích
  + 'global_defs': Đây là phần định nghĩa các biến toàn cầu cho cấu hình Keepalived
    * ‘router_id’ có các giá trị : `LVS_DEVEL`, `LVS_SCHEDULER`, `LVS_METHOD`, `LVS_PERSIST`, `LVS_SYNC` ⇒ sử dụng cái nào cũng được vì nó ko liên quan trực tiếp đến tính năng hoặc chức năng cụ thể của keepalived
  + ‘vrrp_script’: kiểm tra trạng thái của máy chủ Nginx
    * kiểm tra tiến trình nginx có đang hoạt động trong file "/etc/keepalived/check_nginx.sh"   
    * thực hiện kiểm tra trạng thái 2 giây/lần (`interval 2`)
    * xác định mức độ quan trọng của script kiểm tra trong quá trình quyết định máy chủ chính (MASTER) trong nhóm VRRP
  + ‘vrrp_instance’: đại diện cho một nhóm các máy chủ Load Balancer hoạt động cùng nhau
    * ‘state’: Trạng thái "MASTER" hoặc "BACKUP"
    * ‘interface’: Giao diện mạng sử dụng để giao tiếp với các máy chủ khác
    * ‘virtual_router_id’: ID duy nhất để nhận dạng nhóm VRRP Instance => có thể là 1 số bất kỳ
    * ‘priority’: mức độ ưu tiên của máy chủ
    * ‘advert_int’: Thời gian giữa các thông điệp VRRP (advertisement) được gửi (theo giây)
    * ‘virtual_ipaddress’: Địa chỉ IP ảo của nhóm VRRP => được đặt ngẫu nhiên nhưng cùng định dạng với các ip trong hệ thống nhưng là duy nhất của mỗi nhóm VRRP
    * ‘track_script’: chỉ định các script kiểm tra theo dõi để theo dõi trạng thái của các máy chủ Load Balancer
### 7 - Khởi động lại Nginx và Keepalived trên 2 máy load balancer
```
  $ sudo service nginx restart
  $ sudo service keepalived restart
```
### 8 - Check xem ip chung của 2 máy load balancer đã hoạt động chưa
- truy cập vào địa chỉ : http://192.168.188.100 trên trình duyệt
- sau khi refresh trang chúng ta sẽ thấy : 
### 9 - Check hệ thống đảm bảo tính HA
- stop nginx trên máy master
  ```
    $ ip a
  ```
    ![alt text](/images/image1.png)
  ```
    $ sudo service nginx stop
    $ sudo service nginx status
    $ sudo service keepalived status
    $ ip a
  ```
  ![alt text](/images/image2.png)
- check máy backup
```
  $ ip a
```
  ![alt text](/images/image3.png)
- start nginx trên máy master
```
  $ sudo service nginx start
  $ sudo service nginx status
  $ sudo service keepalived status
  $ ip a
```
  ![alt text](/images/image4.png)
- check máy backup
```
  $ ip a
```
![alt text](/images/image5.png)